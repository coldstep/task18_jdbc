# Task #

1. For database "Sample" create DAOs, transformers and services with all CRUD operations. Write a few additional methods (e.g. get all departments, get employee by name). Write a method for department deletion, taking to account the fact that you need to move all employees from that department to another one. Check if your program and database properly works with Cyrillic alphabet.
2. Perform execution of a few query in one transaction. 
3. Write program that read all meta data from a database (e.g. list of tables, parameters, list of cols for each table and so on)
4. Try to create universal transformer using generics, reflections and annotations.
