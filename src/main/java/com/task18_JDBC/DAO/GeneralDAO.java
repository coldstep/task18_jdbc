package com.task18_JDBC.DAO;

import com.task18_JDBC.ConnectionManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public interface GeneralDAO<Entity, ID> {

  List<Entity> selectALL();

  Entity findByID(ID id);

  void update(Entity aEntity);

  void insert(Entity aEntity);

  void delete(Entity aEntity);

  default void getDifficultSelect(String command) {
    try {
      Connection connection = ConnectionManager.getConnection();
      Statement statement = connection.createStatement();
      ResultSet resultSet = statement.executeQuery(command);

      while (resultSet.next()) {
        for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
          System.out.print(resultSet.getString(i)+" ");
        }
        System.out.println();
      }


    } catch (SQLException e) {
      e.printStackTrace();
    }

  }
}
