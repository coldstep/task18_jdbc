package com.task18_JDBC.DAO.DAOimpl;

import com.task18_JDBC.ConnectionManager;
import com.task18_JDBC.DAO.GeneralDAO;
import com.task18_JDBC.model.City;
import com.task18_JDBC.model.Student;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DAOStudentImpl implements GeneralDAO<Student, Long> {

  @Override
  public List<Student> selectALL() {
    List<Student> list = null;
    try {
      Connection connection = ConnectionManager.getConnection();
      Statement statement = connection.createStatement();
      ResultSet resultSet = statement.executeQuery("SELECT * FROM student");
      list = new ArrayList<>();

      while (resultSet.next()) {
        list.add(new Student(
                resultSet.getLong("id"),
                resultSet.getString("surname"),
                resultSet.getString("name"),
                resultSet.getInt("age"),
                resultSet.getString("email"),
                resultSet.getLong("school_id"),
                resultSet.getLong("city_id")
            )
        );
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }

  @Override
  public Student findByID(Long id) {
    List<Student> tempList = new ArrayList<>();
    try {
      Connection connection = ConnectionManager.getConnection();
      PreparedStatement preparedStatement = connection
          .prepareStatement("SELECT * FROM student where id = ? ");
      preparedStatement.setString(1, String.valueOf(id));
      preparedStatement.execute();
      ResultSet resultSet = preparedStatement.getResultSet();
      while (resultSet.next()) {
        tempList.add(new Student(
            resultSet.getLong("id"),
            resultSet.getString("surname"),
            resultSet.getString("name"),
            resultSet.getInt("age"),
            resultSet.getString("email"),
            resultSet.getLong("school_id"),
            resultSet.getLong("city_id")
        ));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return tempList.get(tempList.size() - 1);
  }


  @Override
  public void update(Student aEntity) {
    try {
      Connection connection = ConnectionManager.getConnection();
      PreparedStatement preparedStatement = connection
          .prepareStatement(
              "UPDATE student s "
                  + " SET s.surname = ? , s.name = ? , s.age = ? , s.email = ?, s.school_id = ?, s.city_id = ? "
                  + "WHERE s.id = ?");
      preparedStatement.setString(1, aEntity.getSurname());
      preparedStatement.setString(2, aEntity.getName());
      preparedStatement.setString(3, String.valueOf(aEntity.getAge()));
      preparedStatement.setString(4, aEntity.getEmail());
      preparedStatement.setLong(5, aEntity.getSchool());
      preparedStatement.setLong(6, aEntity.getCity());
      preparedStatement.setLong(7, aEntity.getId());
      preparedStatement.executeUpdate();
      System.out.println("Table updated");
    } catch (SQLException e) {
      System.out.println("Table not updated");
      e.printStackTrace();
    }
  }

  @Override
  public void insert(Student aEntity) {
    try {
      Connection connection = ConnectionManager.getConnection();
      PreparedStatement preparedStatement = connection
          .prepareStatement("INSERT INTO student VALUE (?,?,?,?,?,?,?)");
      preparedStatement.setLong(1, aEntity.getId());
      preparedStatement.setString(2, aEntity.getSurname());
      preparedStatement.setString(3, aEntity.getName());
      preparedStatement.setLong(4, aEntity.getAge());
      preparedStatement.setString(5, aEntity.getEmail());
      preparedStatement.setLong(6, aEntity.getSchool());
      preparedStatement.setLong(7, aEntity.getCity());
      preparedStatement.execute();
      System.out.println("Insert was complete");
    } catch (SQLException e) {
      System.out.println("Insert was failed");
      e.printStackTrace();
    }
  }

  @Override
  public void delete(Student aEntity) {
    try {
      Connection connection = ConnectionManager.getConnection();
      PreparedStatement preparedStatement = connection
          .prepareStatement(
              "DELETE From student "
                  + "WHERE id = ? "
                  + "AND surname = ? "
                  + "AND name = ? "
                  + "AND age = ? "
                  + "AND email = ? "
                  + "AND school_id = ? "
                  + "AND city_id = ?");
      preparedStatement.setLong(1, aEntity.getId());
      preparedStatement.setString(2, aEntity.getSurname());
      preparedStatement.setString(3, aEntity.getName());
      preparedStatement.setLong(4, aEntity.getAge());
      preparedStatement.setString(5, aEntity.getEmail());
      preparedStatement.setLong(6, aEntity.getSchool());
      preparedStatement.setLong(7, aEntity.getCity());
      preparedStatement.execute();
      System.out.println("Delete was complete");
    } catch (SQLException e) {
      System.out.println("Delete was failed");
      e.printStackTrace();
    }
  }
}
