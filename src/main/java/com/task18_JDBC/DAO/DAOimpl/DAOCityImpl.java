package com.task18_JDBC.DAO.DAOimpl;

import com.task18_JDBC.ConnectionManager;
import com.task18_JDBC.DAO.GeneralDAO;
import com.task18_JDBC.model.City;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DAOCityImpl implements GeneralDAO<City, Long> {


  @Override
  public List<City> selectALL() {
    List<City> list = null;
    try {
      Connection connection = ConnectionManager.getConnection();
      Statement statement = connection.createStatement();
      ResultSet resultSet = statement.executeQuery("SELECT * FROM city");
      list = new ArrayList<>();

      while (resultSet.next()) {
        list.add(new City(
                resultSet.getLong("id"),
                resultSet.getString("name")
            )
        );
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }

  @Override
  public City findByID(Long id) {
    List<City> tempList = new ArrayList<>();
    try {
      Connection connection = ConnectionManager.getConnection();
      PreparedStatement preparedStatement = connection
          .prepareStatement("SELECT * FROM city where id = ? ");
      preparedStatement.setString(1, String.valueOf(id));
      preparedStatement.execute();
      ResultSet resultSet = preparedStatement.getResultSet();
      while (resultSet.next()) {
        tempList.add(new City(
            resultSet.getLong("id"),
            resultSet.getString("name")
        ));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return tempList.get(tempList.size() - 1);
  }


  @Override
  public void update(City aEntity) {
    try {
      Connection connection = ConnectionManager.getConnection();
      PreparedStatement preparedStatement = connection
          .prepareStatement("UPDATE city c SET c.name = ? WHERE c.id = ?");
      preparedStatement.setString(1, aEntity.getName());
      preparedStatement.setLong(2, aEntity.getId());
      preparedStatement.executeUpdate();
      System.out.println("Table updated");
    } catch (SQLException e) {
      System.out.println("Table not updated");
      e.printStackTrace();
    }
  }

  @Override
  public void insert(City aEntity) {
    try {
      Connection connection = ConnectionManager.getConnection();
      PreparedStatement preparedStatement = connection
          .prepareStatement("INSERT INTO city VALUE (?,?)");
      preparedStatement.setLong(1, aEntity.getId());
      preparedStatement.setString(2, aEntity.getName());
      preparedStatement.execute();
      System.out.println("Insert was complete");
    } catch (SQLException e) {
      System.out.println("Insert was failed");
      e.printStackTrace();
    }
  }

  @Override
  public void delete(City aEntity) {
    try {
      Connection connection = ConnectionManager.getConnection();
      PreparedStatement preparedStatement = connection
          .prepareStatement("DELETE From city WHERE id = ? AND name = ?");
      preparedStatement.setLong(1, aEntity.getId());
      preparedStatement.setString(2, aEntity.getName());
      preparedStatement.execute();
      System.out.println("Delete was complete");
    } catch (SQLException e) {
      System.out.println("Delete was failed");
      e.printStackTrace();
    }

  }


}
