package com.task18_JDBC.DAO.DAOimpl;

import com.task18_JDBC.ConnectionManager;
import com.task18_JDBC.DAO.GeneralDAO;
import com.task18_JDBC.model.Discipline;
import com.task18_JDBC.model.School;
import com.task18_JDBC.model.Student;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DAOSchoolImpl implements GeneralDAO<School, Long> {

  @Override
  public List<School> selectALL() {
    List<School> list = null;
    try {
      Connection connection = ConnectionManager.getConnection();
      Statement statement = connection.createStatement();
      ResultSet resultSet = statement.executeQuery("SELECT * FROM school");
      list = new ArrayList<>();

      while (resultSet.next()) {
        list.add(new School(
                resultSet.getLong("id"),
                resultSet.getString("name"),
                resultSet.getString("address"),
                resultSet.getString("director")
            )
        );
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }

  @Override
  public School findByID(Long id) {
    List<School> tempList = new ArrayList<>();
    try {
      Connection connection = ConnectionManager.getConnection();
      PreparedStatement preparedStatement = connection
          .prepareStatement("SELECT * FROM school WHERE id = ? ");
      preparedStatement.setString(1, String.valueOf(id));
      preparedStatement.execute();
      ResultSet resultSet = preparedStatement.getResultSet();
      while (resultSet.next()) {
        tempList.add(new School(
            resultSet.getLong("id"),
            resultSet.getString("name"),
            resultSet.getString("address"),
            resultSet.getString("director")
        ));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return tempList.get(tempList.size() - 1);
  }

  @Override
  public void update(School aEntity) {
    try {
      Connection connection = ConnectionManager.getConnection();
      PreparedStatement preparedStatement = connection
          .prepareStatement(
              "UPDATE school s "
                  + " SET s.name = ? , s.address = ? , s.director = ?"
                  + " WHERE s.id = ?");
      preparedStatement.setString(1, aEntity.getName());
      preparedStatement.setString(2, aEntity.getAddress());
      preparedStatement.setString(3, aEntity.getDirector());
      preparedStatement.setLong(4, aEntity.getId());
      preparedStatement.executeUpdate();
      System.out.println("Table updated");
    } catch (SQLException e) {
      System.out.println("Table not updated");
      e.printStackTrace();
    }
  }

  @Override
  public void insert(School aEntity) {
    try {
      Connection connection = ConnectionManager.getConnection();
      PreparedStatement preparedStatement = connection
          .prepareStatement("INSERT INTO school VALUE (?,?,?,?)");
      preparedStatement.setLong(1, aEntity.getId());
      preparedStatement.setString(2, aEntity.getName());
      preparedStatement.setString(3, aEntity.getAddress());
      preparedStatement.setString(4, aEntity.getDirector());
      preparedStatement.execute();
      System.out.println("Insert was complete");
    } catch (SQLException e) {
      System.out.println("Insert was failed");
      e.printStackTrace();
    }
  }

  @Override
  public void delete(School aEntity) {
    try {
      Connection connection = ConnectionManager.getConnection();
      PreparedStatement preparedStatement = connection
          .prepareStatement(
              "DELETE From school WHERE id = ? AND name = ? AND address = ? AND director = ?");
      preparedStatement.setLong(1, aEntity.getId());
      preparedStatement.setString(2, aEntity.getName());
      preparedStatement.setString(3, aEntity.getAddress());
      preparedStatement.setString(4, aEntity.getDirector());
      preparedStatement.execute();
      System.out.println("Delete was complete");
    } catch (SQLException e) {
      System.out.println("Delete was failed");
      e.printStackTrace();
    }
  }

}
