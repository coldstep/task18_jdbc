package com.task18_JDBC.DAO.DAOimpl;

import com.task18_JDBC.ConnectionManager;
import com.task18_JDBC.DAO.GeneralDAO;
import com.task18_JDBC.model.Discipline;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DAODisciplineImpl implements GeneralDAO<Discipline, Long> {

  @Override
  public List<Discipline> selectALL() {
    List<Discipline> list = null;
    try {
      Connection connection = ConnectionManager.getConnection();
      Statement statement = connection.createStatement();
      ResultSet resultSet = statement.executeQuery("SELECT * FROM discipline");
      list = new ArrayList<>();

      while (resultSet.next()) {
        list.add(new Discipline(
                resultSet.getLong("id"),
                resultSet.getString("discipline_name")
            )
        );
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }

  @Override
  public Discipline findByID(Long id) {
    List<Discipline> tempList = new ArrayList<>();
    try {
      Connection connection = ConnectionManager.getConnection();
      PreparedStatement preparedStatement = connection
          .prepareStatement("SELECT * FROM discipline WHERE id = ? ");
      preparedStatement.setString(1, String.valueOf(id));
      preparedStatement.execute();
      ResultSet resultSet = preparedStatement.getResultSet();
      while (resultSet.next()) {
        tempList.add(new Discipline(
            resultSet.getLong("id"),
            resultSet.getString("discipline_name")
        ));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return tempList.get(tempList.size() - 1);
  }

  @Override
  public void update(Discipline aEntity) {
    try {
      Connection connection = ConnectionManager.getConnection();
      PreparedStatement preparedStatement = connection
          .prepareStatement("UPDATE discipline c SET c.discipline_name = ? WHERE c.id = ?");
      preparedStatement.setString(1,aEntity.getDisciplineName());
      preparedStatement.setLong(2,aEntity.getId());
      preparedStatement.executeUpdate();
      System.out.println("Table updated");
    } catch (SQLException e) {
      System.out.println("Table not updated");
      e.printStackTrace();
    }
  }

  @Override
  public void insert(Discipline aEntity) {
    try {
      Connection connection = ConnectionManager.getConnection();
      PreparedStatement preparedStatement = connection
          .prepareStatement("INSERT INTO discipline VALUE (?,?)");
      preparedStatement.setLong(1,aEntity.getId());
      preparedStatement.setString(2,aEntity.getDisciplineName());
      preparedStatement.execute();
      System.out.println("Insert was complete");
    } catch (SQLException e) {
      System.out.println("Insert was failed");
      e.printStackTrace();
    }
  }

  @Override
  public void delete(Discipline aEntity) {
    try {
      Connection connection = ConnectionManager.getConnection();
      PreparedStatement preparedStatement = connection
          .prepareStatement("DELETE From discipline WHERE id = ? AND discipline_name = ?");
      preparedStatement.setLong(1,aEntity.getId());
      preparedStatement.setString(2,aEntity.getDisciplineName());
      preparedStatement.execute();
      System.out.println("Delete was complete");
    } catch (SQLException e) {
      System.out.println("Delete was failed");
      e.printStackTrace();
    }
  }
}
