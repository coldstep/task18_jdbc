package com.task18_JDBC.model;

import java.util.List;

public class Discipline {

  private Long id;
  private String disciplineName;
  private List<Student> students;

  public Discipline() {
  }

  public Discipline(Long id, String disciplineName) {
    this.id = id;
    this.disciplineName = disciplineName;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getDisciplineName() {
    return disciplineName;
  }

  public void setDisciplineName(String disciplineName) {
    this.disciplineName = disciplineName;
  }

  public List<Student> getStudents() {
    return students;
  }

  public void setStudents(List<Student> students) {
    this.students = students;
  }

  @Override
  public String toString() {
    return "Discipline{" +
        "id=" + id +
        ", disciplineName='" + disciplineName + '\'' +
        ", students=" + students +
        '}';
  }
}
