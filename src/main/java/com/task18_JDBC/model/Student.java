package com.task18_JDBC.model;

import java.util.List;

public class Student {

  private Long id;
  private String surname;
  private String name;
  private Integer age;
  private String email;
  private Long school;
  private Long city;
//  private School school;
//  private City city;
//  private List<Discipline> disciplines;

  public Student() {
  }

  public Student(Long id, String surname, String name, Integer age, String email,
      Long school, Long city) {
    this.id = id;
    this.surname = surname;
    this.name = name;
    this.age = age;
    this.email = email;
    this.school = school;
    this.city = city;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getAge() {
    return age;
  }

  public void setAge(Integer age) {
    this.age = age;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Long getSchool() {
    return school;
  }

  public void setSchool(Long school) {
    this.school = school;
  }

  public Long getCity() {
    return city;
  }

  public void setCity(Long city) {
    this.city = city;
  }


  @Override
  public String toString() {
    return "Student{" +
        "id=" + id +
        ", surname='" + surname + '\'' +
        ", name='" + name + '\'' +
        ", age=" + age +
        ", email='" + email + '\'' +
        ", school=" + school +
        ", city=" + city +
        '}';
  }
}
