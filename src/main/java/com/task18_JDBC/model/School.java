package com.task18_JDBC.model;

public class School {

    private Long id;
    private String name;
    private String address;
    private String director;

    public School() {
    }

    public School(Long id, String name, String address, String director) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.director = director;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    @Override
    public String toString() {
        return "School{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", address='" + address + '\'' +
            ", director='" + director + '\'' +
            '}';
    }
}
